/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import modelo.Departamento;
import modelo.Persona;
import vistadatos.Bd;
import vistadatos.Db4o;
import vistapantalla.VistaPrincipal;

/**
 * Fichero: ControladorPrincipal.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-jun-2014
 */
class ControladorPrincipal implements ActionListener {

    private VistaPrincipal vista;
    private ArrayList<Persona> personas = new ArrayList<>();
    private int intentos = 0;
    private ControladorPersona ControladorPersona;

    // private ControladorIndependiente control = new ControladorIndependiente();
    // Recoge lo que le pasa el Main a través del constructor
    // Guarda lo que le pasan en el Main variable vista y personas del controlador
    // principal
    ControladorPrincipal(VistaPrincipal vista, ArrayList<Persona> personas) {

        this.vista = vista;
        this.personas = personas;

        inicializabotones();
        inicializaobjetos();
        grabarPersonas();
        visualizar();

    }

    // que se vea la ventana
    public void visualizar() {
        vista.setVisible(true);
    }

    public void inicializabotones() {

        //  Etiquetar el botón
        vista.getBt1().setActionCommand("Entrar");
        // this es la propia clase ControladorPrincipal
        vista.getBt1().addActionListener(this);
        // Poner lo que se se en pantalla
        vista.getBt1().setText("Entrar");

        vista.getBt2().setActionCommand("Salir");
        vista.getBt2().addActionListener(this);
        vista.getBt2().setText("Salir");

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        /* recojo lo del action event ej. boton GrabarVistapersona,
         lo guardo en el string commando y lo procesa el switch
         */
        String comando = ae.getActionCommand();

        switch (comando) {
            case "Entrar":
                entrar();
                break;
            case "Salir":
                salir();
                break;
            case "SalirVistaPersona":
                ControladorPersona.SalirVistaPersona();
                break;
            case "GrabarVistaPersona":
                ControladorPersona.GrabarVistaPersona();
                break;
        } // swith

    }

    public void entrar() {
        // no hace falta
        // Boolean usuarioCorrecto = false;

        String usuario = vista.getJtf1().getText();
        String password = vista.getJtf2().getText();

        Persona personabuscada = new Persona(null, usuario, password, null);
        Persona persona;

        persona = buscarPersona(personabuscada);

        if (persona != null) {
            ControladorPersona = new ControladorPersona(persona, vista, this);
            ControladorPersona.visualizar();
        } else { // No es correcto.
            intentos++;
            vista.getJl3().setText("Error: Usuario Incorrecto. Intento:" + intentos);
        }

        if (intentos >= 3) {
            salir();
        }

    }

    public void salir() {
        vista.dispose();
    }

    public void SalirVistaPersona() {
        vista.dispose();
    }

    public void inicializaobjetos() {

        Departamento d1 = new Departamento("d1", "Inf");

        Persona p1 = new Persona("p1", "admin", "123", d1);
        Persona p2 = new Persona("p2", "paco", "123", d1);

        personas.add(p1);
        personas.add(p2);

    }

    public Persona buscarPersona(Persona personabuscada) {

        Iterator it = personas.iterator();
        while (it.hasNext()) {
            Persona persona = (Persona) it.next();
            if (personabuscada.getUsuario().equals(persona.getUsuario()) && personabuscada.getPassword().equals(persona.getPassword())) {
                return persona;
            }
        }
        return null;
    }

    public void grabarPersonas() {
        // se puede quitar
        if (personas.size() == 0) {
            return;
        }

        Bd bd = new Db4o();
        bd.open();
        //
        Iterator it = personas.iterator();

        // Mientras exista lo Recupero la persona y la grabo. y avanzas al siguiente.
        while (it.hasNext()) {
            Persona persona = (Persona) it.next();
            bd.escribir(persona);
        }
        bd.close();
    }
}
