package controlador;

import java.util.ArrayList;
import modelo.Persona;
import vistapantalla.VistaPrincipal;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @autor Juan Antonio Bleda juanbledap@gmail.com // comentarios
 * @date 02-jun-2014
 */
public class Main {
    /*
     Creo  tres objetos modelo, vista  y controlador
     el controlador conecta la vista y el modelo
     */

    public static void main(String[] args) {
        ArrayList<Persona> modelo = new ArrayList<>();
        VistaPrincipal vista = new VistaPrincipal();
        ControladorPrincipal controlador = new ControladorPrincipal(vista, modelo);
    }
}
