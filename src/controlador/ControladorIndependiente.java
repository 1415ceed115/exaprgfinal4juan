/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fichero: ControladorIndependiente.java
 * @author Juan A. Bleda Perez <juanbledap@gmail.com>
 * @date 14-may-2015
 */
public class ControladorIndependiente implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
    }

}
